import wasm from "@anagolay/wf-cid/workflow_compute_cidv1_from_bytes_bg.wasm?url";
import init, { Workflow } from "@anagolay/wf-cid";

onmessage = async function (message) {
  const start = new Date().getTime();

  await init(wasm);
  const wf = new Workflow();
  const { output } = await wf.next([message.data]);
  const cid = output.toString();
  const elapsedTime = new Date().getTime() - start + " ms";
  self.postMessage({ time: elapsedTime, cid, name: "WebWorker" });
};
