import { writable, type Subscriber, type Unsubscriber } from 'svelte/store';

export interface IProcess {
  time: string,
  name: string,
  cid: string
}

function procesStoreFn() {
  const { subscribe, set, update } = writable<IProcess[]>([]);
  
  set([])

  return {
    subscribe,
    set,
    add: (process: IProcess) => {
      return update((currentState) => {
        let cs = [...currentState];
        cs.push(process)
        currentState = [...cs];
        return currentState;
      });
    },
  };
}


/**
 * Selected statuses Store instance
 */
export const processStore = procesStoreFn();
