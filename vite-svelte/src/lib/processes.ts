import { processStore } from "$src/stores";
import { invoke } from "@tauri-apps/api";
import wasm from "@anagolay/wf-cid/workflow_compute_cidv1_from_bytes_bg.wasm?url";
import init, { Workflow } from "@anagolay/wf-cid";

export interface ProcessCidWithPerformance {
  time: string;
  cid: string;
}


const worker = new Worker(new URL("../worker.js", import.meta.url), {
  type: "module",
});

export async function tauriCid(data: Uint8Array) {
  const newData = Array.from(data);
  const start = new Date().getTime();

  const output = await invoke("calculate_cid", {
    data: newData,
  });

  console.log('output',output);
  
  const cid = output[0].toString();
  const elapsedTime = new Date().getTime() - start + " ms";
  
  // this needs to be first becuse it caches it somehow and it's 0
  processStore.add({ time: output[1].toString(), cid, name: "Tauri Rust" })

  // wrapped 
  processStore.add({ time: elapsedTime, cid, name: "Tauri Rust via JS" })
}


export async function tauriMainThread(data: Uint8Array) {
  const start = new Date().getTime();

  await init(wasm);

  const wf = new Workflow();
  const { output } = await wf.next([data]);
  const cid = output.toString();
  const elapsedTime = new Date().getTime() - start + " ms";

  processStore.add({ time: elapsedTime, cid, name: "Main thread" })
}
export async function tauriWebWorker(data: Uint8Array) {

  worker.postMessage(data);
  worker.onmessage = (message) => {
    processStore.add(message.data)
  }


}