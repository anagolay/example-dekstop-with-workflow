const { getLineAndCharacterOfPosition } = require("typescript");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: [
      {
        light: {
          primary: "#23ACF6",
          "primary-content": "#F4FBFF",
          secondary: "#054F77",
          accent: "#8CFF00",
          neutral: "#3D4451",
          "base-100": "#E5E5E5",
          "base-content": "#141F45",
          info: "#3ABFF8",
          success: "#36D399",
          warning: "#FBBD23",
          error: "#F87272",
        },
      },
    ],
  },
  plugins: [require("daisyui")],
};
