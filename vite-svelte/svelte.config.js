import sveltePreprocess from "svelte-preprocess";
import { resolve } from "node:path";

export default {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: sveltePreprocess(),
};
