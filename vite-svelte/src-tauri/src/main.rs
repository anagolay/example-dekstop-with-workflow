#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use core::any::Any;
use std::rc::Rc;
use std::time::Instant;
use workflow_compute_cidv1_from_bytes::Workflow;

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![greet])
        .invoke_handler(tauri::generate_handler![calculate_cid])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}!", name)
}

#[tauri::command]
fn calculate_cid(data: Vec<u8>) -> (String, String) {
    let start = Instant::now();

    let workflow = Workflow::new();
    let inputs: Vec<Rc<dyn Any>> = vec![Rc::new(data)];

    let result = futures::executor::block_on(workflow.next(inputs)).unwrap();
    let result = result.as_ref();
    assert!(result.is_done(), "Workflow is not finished");

    let output = result
        .get_output()
        .unwrap()
        .downcast_ref::<String>()
        .unwrap()
        .clone();

    // let time = result.get_total_time().to_string();
    let duration = start.elapsed();
    // to get milis but with more precision
    let elapsed = duration.subsec_nanos() / 1000000;

    println!("elapsed time: {:?} ms", &duration);
    (output, elapsed.to_string())
}
